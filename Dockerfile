FROM golang:1.14.1-alpine3.11@sha256:3e935ab77ba5d71c7778054fbb60c029c1564b75266beeeb4223aa04265e16c1 AS build

RUN apk add --no-cache \
    gcc \
    git \
    musl-dev
COPY go /go
WORKDIR /go/src/github.com/linuxkit/linuxkit/src/cmd/linuxkit
RUN go get 


FROM docker:19.03.8@sha256:2b53171de8f42890031a42d014d2e18faa11a077f4444993ab3a32bd67b0e76e

COPY --from=build /go/bin/linuxkit /usr/bin/linuxkit

ENTRYPOINT ["/usr/bin/linuxkit"]
